package encryption;

import com.axxiome.encryption.EncodingUtils;
import com.axxiome.encryption.SymmetricBox;
import org.abstractj.kalium.NaCl;
import org.abstractj.kalium.crypto.Random;
import org.junit.Test;

import static org.abstractj.kalium.NaCl.Sodium.CRYPTO_SECRETBOX_XSALSA20POLY1305_KEYBYTES;
import static org.abstractj.kalium.NaCl.Sodium.CRYPTO_SECRETBOX_XSALSA20POLY1305_NONCEBYTES;
import static org.junit.Assert.assertEquals;

/**
 * Created by RWa on 2016-08-17.
 */
public class SymmetricEncryptionDemoTest {

    @Test
    public void encryptingAndThenDecryptingSourceMessageShouldReturnSourceMessage() {

        //given
        String sourceMessage = "Test source message";

        //http://security.stackexchange.com/questions/3001/what-is-the-use-of-a-client-nonce -- about using NONCE
        //This number should be used ONCE for every message, it should be send together with a request Server <-> Client
        Random randomGenerator = new Random();
        byte[] nonceBytes = randomGenerator.randomBytes(NaCl.Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_NONCEBYTES);
        String nonceHex = EncodingUtils.encodeHex(nonceBytes);

        String secretKeySeed = EncodingUtils.encodeHex(randomGenerator.randomBytes(CRYPTO_SECRETBOX_XSALSA20POLY1305_KEYBYTES));

        //Creating secretKeyPair
        SymmetricBox symmetricBox = new SymmetricBox(secretKeySeed);

        //when
        String encryptedMessage = symmetricBox.encryptMessage(nonceHex, sourceMessage);
        String decryptedMessage = symmetricBox.decryptMessage(nonceHex, encryptedMessage);

        //then
        assertEquals(sourceMessage, decryptedMessage);
    }
}