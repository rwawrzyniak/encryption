package encryption;

import com.axxiome.encryption.AsymetricDecryptionBox;
import com.axxiome.encryption.AsymetricEncryptionBox;
import com.axxiome.encryption.EncodingUtils;
import org.abstractj.kalium.NaCl;
import org.abstractj.kalium.crypto.Random;
import org.abstractj.kalium.keys.KeyPair;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AsymmetricEncryptionDemoTest {

    @Test
    public void encryptingAndThenDecryptingSourceMessageShouldReturnSourceMessage(){

        //given
        String sourceMessage = "Test source message";

        //http://security.stackexchange.com/questions/3001/what-is-the-use-of-a-client-nonce -- about using NONCE
        //This number should be used ONCE for every message, it should be send together with a request Server <-> Client
        Random randomGenerator = new Random();
        byte[] nonceBytes = randomGenerator.randomBytes(NaCl.Sodium.CRYPTO_BOX_CURVE25519XSALSA20POLY1305_NONCEBYTES);
        String nonceHex = EncodingUtils.encodeHex(nonceBytes);

        //Creating keyPairs
        KeyPair aliceKeyPair = new KeyPair(randomGenerator.randomBytes());
        KeyPair bobKeyPair = new KeyPair(randomGenerator.randomBytes());
        String alicePublicKeyHex = EncodingUtils.encodeHex(aliceKeyPair.getPublicKey().toBytes());
        String bobPublicKeyHex = EncodingUtils.encodeHex(bobKeyPair.getPublicKey().toBytes());

        AsymetricEncryptionBox aliceBox = new AsymetricEncryptionBox(aliceKeyPair, bobPublicKeyHex);
        AsymetricDecryptionBox bobBox = new AsymetricDecryptionBox(bobKeyPair, alicePublicKeyHex);

        //when
        String encryptedMessage = aliceBox.encryptMessage(sourceMessage,nonceHex);
        String decryptedMessage = bobBox.decryptMessage(encryptedMessage,nonceHex);

        //then
        assertEquals(sourceMessage,decryptedMessage);
    }
}
