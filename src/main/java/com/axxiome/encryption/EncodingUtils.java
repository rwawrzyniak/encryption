package com.axxiome.encryption;

import org.abstractj.kalium.encoders.Hex;

public class EncodingUtils {
    private static Hex hexEncoder = new Hex();

    public static String encodeHex(byte[] bytes) {
        return hexEncoder.encode(bytes);
    }

    public static byte[] decodeHex(String message) {
        return hexEncoder.decode(message);
    }
}
