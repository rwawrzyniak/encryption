package com.axxiome.encryption;

import org.abstractj.kalium.crypto.SecretBox;

public class SymmetricBox {

    private SecretBox secretBox;

    public SymmetricBox(String secretKey){
        this.secretBox = new SecretBox(EncodingUtils.decodeHex(secretKey));
    }

    public String encryptMessage(String nonce, String message){
        System.out.println("Message before message: " + message);
        return EncodingUtils.encodeHex(this.secretBox.encrypt(EncodingUtils.decodeHex(nonce), message.getBytes()));
    }

    public String decryptMessage(String nonce, String message){
        return new String(this.secretBox.decrypt(EncodingUtils.decodeHex(nonce), EncodingUtils.decodeHex(message)));
    }
}