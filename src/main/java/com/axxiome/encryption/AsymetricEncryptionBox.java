package com.axxiome.encryption;

import org.abstractj.kalium.crypto.Box;
import org.abstractj.kalium.keys.KeyPair;

public class AsymetricEncryptionBox {

    private KeyPair keyPair;
    private Box cryptoBox;

    public AsymetricEncryptionBox(KeyPair keyPair, String otherPartyPK){
        this.keyPair = keyPair;
        this.cryptoBox = new Box(EncodingUtils.decodeHex(otherPartyPK), keyPair.getPrivateKey().toBytes());
    }

    public String encryptMessage(String message, String nonce){
        System.out.println("Message before encrypting: " + message);
        System.out.println("Encryption side private key:: " + keyPair.getPrivateKey());
        System.out.println("Encryption side public key " + keyPair.getPublicKey());

        byte[] encryptedMessageBytes = this.cryptoBox.encrypt(EncodingUtils.decodeHex(nonce),message.getBytes());
        return EncodingUtils.encodeHex(encryptedMessageBytes);
    }
}
