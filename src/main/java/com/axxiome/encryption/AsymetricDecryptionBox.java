package com.axxiome.encryption;

import org.abstractj.kalium.crypto.Box;
import org.abstractj.kalium.keys.KeyPair;

public class AsymetricDecryptionBox {

    private KeyPair keyPair;
    private Box cryptoBox;

    public AsymetricDecryptionBox(KeyPair keyPair, String otherPartyPK){
        this.keyPair = keyPair;
        this.cryptoBox = new Box(EncodingUtils.decodeHex(otherPartyPK), keyPair.getPrivateKey().toBytes());
    }

    public String decryptMessage(String message, String nonce){

        System.out.println("Before decryption message in hex is: " + message);
        System.out.println("Decryption side private key:: " + keyPair.getPrivateKey());
        System.out.println("Decryption side public key " + keyPair.getPublicKey());

        byte[] messageInBytes = EncodingUtils.decodeHex(message);
        byte[] nonceInBytes = EncodingUtils.decodeHex(nonce);
        String decryptedMessage = new String(this.cryptoBox.decrypt(nonceInBytes,messageInBytes));

        System.out.println("After decryption message is: " + decryptedMessage);

        return decryptedMessage;
    }
}
